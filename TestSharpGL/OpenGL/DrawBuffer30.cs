﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using SharpGL;

namespace TestSharpGL
{
    class DrawBuffer30 : DrawBuffer15, IDrawBuffer
    {
        private const uint cAttrPosition = 0;
        private const uint cAttrColor = 1;
        private uint mProgramHandle;


        public DrawBuffer30() : base()
        {
            mProgramHandle = 0;
        }

        private void Apply(OpenGL gl)
        {
            gl.EnableVertexAttribArray(cAttrPosition);
            gl.VertexAttribPointer(cAttrPosition, 2, OpenGL.GL_FLOAT, false, cVertexSize, new IntPtr(0));

            gl.EnableVertexAttribArray(cAttrColor);
            gl.VertexAttribPointer(cAttrColor, 4, OpenGL.GL_UNSIGNED_BYTE, false, cVertexSize, new IntPtr(8));
        }

        private static uint CompileShader(OpenGL gl, string aShaderSource, uint aShaderType)
        {
            uint shaderHandle = gl.CreateShader(aShaderType);
            gl.ShaderSource(shaderHandle, aShaderSource);
            gl.CompileShader(shaderHandle);

            var p = new int[1] { 0 };
            gl.GetShader(shaderHandle, OpenGL.GL_COMPILE_STATUS, p);
            if (p[0] == 0) return 0;

            return shaderHandle;
        }

        private bool CompileProgram(OpenGL gl)
        {
            mProgramHandle = 0;

            const string cVertexShader = @"
attribute vec2 a_Position;
attribute vec4 a_Color;

varying lowp vec4 frag_Color;

void main(void) {
    frag_Color = a_Color;
    gl_Position = vec4(a_Position, 0, 1);
}
";

        const string cFragmentShader = @"
varying lowp vec4 frag_Color;

void main(void) {
    gl_FragColor = frag_Color;
}
";

        uint vertexShaderId = CompileShader(gl, cVertexShader, OpenGL.GL_VERTEX_SHADER);
            uint fragmentShaderId = CompileShader(gl, cFragmentShader, OpenGL.GL_FRAGMENT_SHADER);
            if ((vertexShaderId == 0) || (fragmentShaderId == 0)) return false;

            mProgramHandle = gl.CreateProgram();
            gl.AttachShader(mProgramHandle, vertexShaderId);
            gl.AttachShader(mProgramHandle, fragmentShaderId);

            gl.BindAttribLocation(mProgramHandle, cAttrPosition, "a_Position");
            gl.BindAttribLocation(mProgramHandle, cAttrColor, "a_Color");
            gl.LinkProgram(mProgramHandle);

            var linkStatus = new int[1] { 0 };
            gl.GetProgram(mProgramHandle, OpenGL.GL_LINK_STATUS, linkStatus);
            return linkStatus[0] != 0;
        }

        public override void Draw(OpenGL gl)
        {
            gl.UseProgram(mProgramHandle);
            base.Draw(gl);
        }

        public override void Init(OpenGL gl)
        {
            base.Init(gl);
            Apply(gl);
            CompileProgram(gl);
        }
    }
}























