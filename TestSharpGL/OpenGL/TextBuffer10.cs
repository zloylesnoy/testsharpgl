﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    class TextBuffer10 : ATextBuffer, ITextBuffer
    {
        public TextBuffer10(IFontTexture aFontTexture) :
            base(aFontTexture)
        {
        }

        public void Init(OpenGL gl)
        {
            //  Ничего не делает.
        }

        public void Done(OpenGL gl)
        {
            //  Ничего не делает.
        }

        public void Draw(OpenGL gl)
        {
            foreach (var f in mTextDraws)
            {
                f.DrawBeginEnd(gl);
            }
        }
    }
}


















