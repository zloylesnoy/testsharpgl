﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    interface ITextBuffer : IBuffer
    {
        void Add(ITextDraw aTextDraw);
    }
}
