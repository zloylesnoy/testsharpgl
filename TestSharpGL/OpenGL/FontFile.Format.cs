﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    partial class FontFile
    {
        //  Заголовок файла, занимает в файле 32 байт.
        private struct SFontFileHeader
        {
            public const UInt32 eSignatureValue = 0x544E4F46; // "FONT"
            public const UInt16 eVersion1 = 1;

            public UInt32 mSignature;  // всегда eSignatureValue
            public UInt32 mFileSize;   // размер файла в байтах
            public UInt32 mCrcCode;    // пока не используется
            public UInt16 mVersion;    // версия формата, пока eVersion1
            public UInt16 mFontCount;  // число записей в таблице SFontRecord[]
            public UInt32 mFontOffset; // смещение таблицы SFontRecord[] от начала файла
            public UInt16 mTexCount;   // число используемых текстур
            public UInt16 mTexLevel;   // если не 0, то mWidth == mHeight == (1 << mTexLevel)
            public UInt32 mWidth;      // ширина текстур
            public UInt32 mHeight;     // высота текстур

            public static int SizeOf()
            {
                return 32;
            }

            public bool Load(byte[] aData, int aOffset)
            {
                if (aData == null) return false;
                if (aOffset < 0) return false;
                if (aOffset + 32 > aData.Length) return false;
                int p = aOffset;

                mSignature = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mFileSize = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mCrcCode = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mVersion = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mFontCount = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mFontOffset = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mTexCount = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mTexLevel = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mWidth = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mHeight = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                if (mSignature != eSignatureValue) return false;
                if (mVersion != eVersion1) return false;
                return true;
            }
        }

        //  Описание одного шрифта, занимает в файле 64 байт.
        private struct SFontRecord
        {
            public const int eFontNameLength = 44;

            public UInt32 mGlyphOffset; // смещение таблицы SGlyphRecord[] от начала файла
            public UInt32 mGlyphCount;  // число записей в таблице SGlyphRecord[]
            public UInt32 mPairOffset;  // смещение таблицы SKerningPair[] от начала файла
            public UInt32 mPairCount;   // число записей в таблице SKerningPair[]
            public Int16 mUpBorder;     // самый верхний тексел относительно курсора
            public Int16 mDownBorder;   // самый нижний тексел относительно курсора

            //  имя шрифта в движке, в файле хранится как ASCII с завершающим 0
            //  в формате char[eFontNameLength], где char занимает 1 байт.
            public string mName;

            public static int SizeOf()
            {
                return 64;
            }

            public bool Load(byte[] aData, int aOffset)
            {
                if (aData == null) return false;
                if (aOffset < 0) return false;
                if (aOffset + 8 > aData.Length) return false;
                int p = aOffset;

                mGlyphOffset = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mGlyphCount = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mPairOffset = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mPairCount = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mUpBorder = System.BitConverter.ToInt16(aData, p);
                p += 2;

                mDownBorder = System.BitConverter.ToInt16(aData, p);
                p += 2;

                var sb = new StringBuilder();
                for (int i = 0; i < eFontNameLength; ++i)
                {
                    byte ascii = aData[p + i];
                    if (ascii == 0) break;
                    sb.Append(Char.ConvertFromUtf32(ascii));
                }
                mName = sb.ToString();
                p += eFontNameLength;

                return true;
            }
        }

        //  Кернинг двух символов, занимает в файле 8 байт.
        private struct SKerningPair
        {
            public UInt32 mFirstChar;  // первый символ пары, старшие 24 бита
            public UInt32 mSecondChar; // второй символ пары, следующие 24 бита
            public Int16 mKernAmount;  // смещение со знаком, младшие 16 бит

            public static int SizeOf()
            {
                return 8;
            }

            public bool Load(byte[] aData, int aOffset)
            {
                if (aData == null) return false;
                if (aOffset < 0) return false;
                if (aOffset + 8 > aData.Length) return false;

                UInt64 u = BitConverter.ToUInt64(aData, aOffset);
                mFirstChar = (UInt32)(u >> 40);
                mSecondChar = (UInt32)(u >> 16) & 0x00FFFFFF;
                mKernAmount = (Int16)u;
                return true;
            }
        }

        //  Описание одного символа, занимает в файле 24 байт.
        private struct SGlyphRecord
        {
            public UInt32 mSymbol;           // код символа UNICODE

            public UInt16 mType;             // тип символа, смотри константы ниже:
            public const UInt16 eSpaceType = 0; // символ пробельный, то есть ничего не выводится
            // на экран и mTexture=mTexX=mTexY=mBoxX=mBoxY=mOriginX=mOriginY=0
            public const UInt16 eNormalType = 1; // обычный отображаемый символ
            public const UInt16 eCombiningType = 2; // закорючка к символу

            public UInt16 mTexture;          // номер текстуры
            public UInt16 mTexX, mTexY;      // размещение фрагмента текстуры
            public UInt16 mBoxX, mBoxY;      // размер фрагмента текстуры (без полей)
            public Int16 mOriginX, mOriginY; // координаты фрагмента текстуры относительно курсора
            public Int16 mShiftX, mShiftY;   // смещение курсора после вывода символа, обычно shiftY=0

            public static int SizeOf()
            {
                return 24;
            }

            public bool Load(byte[] aData, int aOffset)
            {
                if (aData == null) return false;
                if (aOffset < 0) return false;
                if (aOffset + 8 > aData.Length) return false;
                int p = aOffset;

                mSymbol = System.BitConverter.ToUInt32(aData, p);
                p += 4;

                mType = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mTexture = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mTexX = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mTexY = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mBoxX = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mBoxY = System.BitConverter.ToUInt16(aData, p);
                p += 2;

                mOriginX = System.BitConverter.ToInt16(aData, p);
                p += 2;

                mOriginY = System.BitConverter.ToInt16(aData, p);
                p += 2;

                mShiftX = System.BitConverter.ToInt16(aData, p);
                p += 2;

                mShiftY = System.BitConverter.ToInt16(aData, p);
                p += 2;

                return true;
            }

            public bool IsSpace()
            {
                return mType == eSpaceType;
            }

            public bool IsNormal()
            {
                return mType == eNormalType;
            }

            public bool IsCombining()
            {
                return mType == eCombiningType;
            }
        }

        private bool LoadIndexData(byte[] aData)
        {
            Clear();
            var header = new SFontFileHeader();
            if (!header.Load(aData, 0)) return false;
            if (header.mFileSize != aData.Length) return false;

            mTexCount = header.mTexCount;
            mTexLevel = header.mTexLevel;
            mTexelWidth  = 1 / (float)header.mWidth;
            mTexelHeight = 1 / (float)header.mHeight;

            for (int i = 0; i < header.mFontCount; ++i)
            {
                var fontRec = new SFontRecord();
                if (!fontRec.Load(aData, (int)header.mFontOffset + i * SFontRecord.SizeOf())) return false;

                var font = new SFont();
                font.mName = fontRec.mName;
                font.mUpBorder = fontRec.mUpBorder;
                font.mDownBorder = fontRec.mDownBorder;
                font.mKerning = new Dictionary<UInt32, Dictionary<UInt32, Int16>>();
                font.mGlyphs = new Dictionary<UInt32, SGlyphRecord>();

                for (int j = 0; j < fontRec.mPairCount; ++j)
                {
                    var kp = new SKerningPair();
                    if (!kp.Load(aData, (int)fontRec.mPairOffset + j * SKerningPair.SizeOf())) return false;
                    if (!font.mKerning.ContainsKey(kp.mFirstChar))
                    {
                        font.mKerning[kp.mFirstChar] = new Dictionary<uint, short>();
                    }
                    font.mKerning[kp.mFirstChar][kp.mSecondChar] = kp.mKernAmount;
                }

                for (int j = 0; j < fontRec.mGlyphCount; ++j)
                {
                    var gr = new SGlyphRecord();
                    if (!gr.Load(aData, (int)fontRec.mGlyphOffset + j * SGlyphRecord.SizeOf())) return false;
                    font.mGlyphs[gr.mSymbol] = gr;
                }

                mFonts.Add(font);
            }

            return true;
        }
    }
}

















