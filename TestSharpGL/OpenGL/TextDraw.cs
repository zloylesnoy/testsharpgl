﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования текста, соответствует одному вызову функции
    //  glDrawArray(GL_TRIANGLES, ...) для каждой текстуры шрифта.
    //  Рисует набор текстурированных треугольников с прозрачностью.
    //  Неизвестно в каком порядке будут рисоваться треугольники из одного набора.
    class TextDraw : ITextDraw
    {
        protected readonly IFontTexture mFontTexture;

        //  Для каждой текстуры шрифта один элемент.
        private TextDrawCall[] mPerTexture;

        private int mCursorX;
        private int mCursorY;


        public TextDraw(IFontTexture aFontTexture)
        {
            mFontTexture = aFontTexture;
            int n = aFontTexture.TextureCount();
            mPerTexture = new TextDrawCall[n];
            for (int i = 0; i < n; i++) mPerTexture[i] = new TextDrawCall();
            mCursorX = 0;
            mCursorY = 0;
        }

        public int VertexCount(int aIndex)
        {
            if ((aIndex < 0) || (aIndex >= mPerTexture.Length)) return 0;
            return mPerTexture[aIndex].VertexCount();
        }

        public int VertexCount()
        {
            return mPerTexture.Select(x => x.VertexCount()).Sum();
        }

        //  Записать данные в один общий буфер по смещению aOffsetInBuffer вершин.
        //  Запомнить смещение aOffsetInBuffer, чтобы позже использовать при рисовании.
        public int WriteToBuffer(int aOffsetInBuffer, byte[] aBuffer)
        {
            int ofs = aOffsetInBuffer;
            for (int i = 0; i < mPerTexture.Length; i++)
            {
                ofs = mPerTexture[i].WriteToBuffer(ofs, aBuffer);
            }
            return ofs;
        }

        public void Draw(OpenGL gl)
        {
            for (int i = 0; i < mPerTexture.Length; i++)
            {
                if (mPerTexture[i].VertexCount() == 0) continue;
                mFontTexture.UseTexture(gl, i);
                mPerTexture[i].Draw(gl);
            }
            mFontTexture.DontUseTexture(gl);
        }

        public void DrawBeginEnd(OpenGL gl)
        {
            for (int i = 0; i < mPerTexture.Length; i++)
            {
                if (mPerTexture[i].VertexCount() == 0) continue;
                mFontTexture.UseTexture(gl, i);
                mPerTexture[i].DrawBeginEnd(gl);
            }
            mFontTexture.DontUseTexture(gl);
        }

        //  Установить текущий цвет.
        public void SetColor(byte aRed, byte aGreen, byte aBlue, byte aAlpha)
        {
            for (int i = 0; i < mPerTexture.Length; i++)
            {
                mPerTexture[i].SetColor(aRed, aGreen, aBlue, aAlpha);
            }
        }

        public void GetCursor(out int aX, out int aY)
        {
            aX = mCursorX;
            aY = mCursorY;
        }

        public void SetCursor(int aX, int aY)
        {
            mCursorX = aX;
            mCursorY = aY;
        }

        public void AddVertex(int aIndex, float aX, float aY, float aU, float aV)
        {
            mPerTexture[aIndex].AddVertex(aX, aY, aU, aV);
        }

        public void Text(int aFontId, string aText)
        {
            uint prevChar = 0;
            foreach (var ch in aText)
            {
                mFontTexture.WriteChar(aFontId, ch, prevChar, this);
                prevChar = ch;
            }
        }
    }
}




















