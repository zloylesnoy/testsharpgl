﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования, соответствует одному вызову функции glDrawArray(GL_LINE_STRIP, ...).
    //  Рисует одну ломаную фиксированной толщины. Цвет линии может меняться.
    class DrawLineStrip : ADrawCall, IDrawCall
    {
        //  Толщина линии.
        private float mLineWidth;


        public DrawLineStrip(float aLineWidth) : base()
        {
            mLineWidth = aLineWidth;
        }

        public void Draw(OpenGL gl)
        {
            gl.LineWidth(mLineWidth);
            gl.DrawArrays(OpenGL.GL_LINE_STRIP, OffsetInBuffer(), VertexCount());
        }

        public void DrawBeginEnd(OpenGL gl)
        {
            gl.LineWidth(mLineWidth);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            InsideBeginEnd(gl);
            gl.End();
        }

        //  Добавить одну вершину к ломаной линии.
        public void LineTo(float aX, float aY)
        {
            AddVertex(aX, aY);
        }
    }
}
