﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования текста, соответствует одному вызову функции
    //  glDrawArray(GL_TRIANGLES, ...) для одной текстуры шрифта.
    //  Рисует набор текстурированных треугольников с прозрачностью.
    //  Неизвестно в каком порядке будут рисоваться треугольники из одного набора.
    class TextDrawCall : ADrawCall
    {
        //  Текстурные координаты вершин, U и V для каждой вершины.
        private List<float> mUvList;


        public TextDrawCall() : base()
        {
            mUvList = new List<float>();
        }

        //  Записать данные в один общий буфер по смещению aOffsetInBuffer вершин.
        //  Запомнить смещение aOffsetInBuffer, чтобы позже использовать при рисовании.
        public override int WriteToBuffer(int aOffsetInBuffer, byte[] aBuffer)
        {
            const int cVertexSize = 20;
            int nVertex = VertexCount();

            mOffsetInBuffer = aOffsetInBuffer;
            var vc = aOffsetInBuffer * cVertexSize;
            var end = vc + nVertex * cVertexSize;
            int v2 = 0;
            int c2 = 0;
            int t2 = 0;
            
            while (vc < end)
            {
                var xs = BitConverter.GetBytes(mXyList[v2++]);
                aBuffer[vc++] = xs[0];
                aBuffer[vc++] = xs[1];
                aBuffer[vc++] = xs[2];
                aBuffer[vc++] = xs[3];

                var ys = BitConverter.GetBytes(mXyList[v2++]);
                aBuffer[vc++] = ys[0];
                aBuffer[vc++] = ys[1];
                aBuffer[vc++] = ys[2];
                aBuffer[vc++] = ys[3];

                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];

                var us = BitConverter.GetBytes(mUvList[t2++]);
                aBuffer[vc++] = us[0];
                aBuffer[vc++] = us[1];
                aBuffer[vc++] = us[2];
                aBuffer[vc++] = us[3];

                var vs = BitConverter.GetBytes(mUvList[t2++]);
                aBuffer[vc++] = vs[0];
                aBuffer[vc++] = vs[1];
                aBuffer[vc++] = vs[2];
                aBuffer[vc++] = vs[3];
            }
            return aOffsetInBuffer + nVertex;
        }
        
        //  Выполнить команду.
        public void Draw(OpenGL gl)
        {
            gl.DrawArrays(OpenGL.GL_TRIANGLES, OffsetInBuffer(), VertexCount());
        }

        //  Выполнить команду, используя glBegin - glEnd.
        public void DrawBeginEnd(OpenGL gl)
        {
            int n = mXyList.Count;
            if (n == 0) return;

            gl.Begin(OpenGL.GL_TRIANGLES);
            for (int i = 0; i < n; i += 2)
            {
                gl.TexCoord(mUvList[i], mUvList[i + 1]);
                gl.Color(mColorList[i * 2], mColorList[i * 2 + 1], mColorList[i * 2 + 2], mColorList[i * 2 + 3]);
                gl.Vertex(mXyList[i], mXyList[i + 1]);
            }
            gl.End();
        }

        //  Добавить одну вершину текущего цвета.
        public void AddVertex(float aX, float aY, float aU, float aV)
        {
            mUvList.Add(aU);
            mUvList.Add(aV);
            AddVertex(aX, aY);
        }
    }
}
