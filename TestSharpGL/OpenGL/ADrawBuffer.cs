﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    abstract class ADrawBuffer
    {
        //  Размер вершины в байтах.
        protected const int cVertexSize = 12;

        protected List<IDrawCall> mDrawCalls;


        public ADrawBuffer()
        {
            mDrawCalls = new List<IDrawCall>();
        }

        //  Добавлять DrawCall можно только до вызова функции Create.
        public void Add(IDrawCall aDrawCall)
        {
            mDrawCalls.Add(aDrawCall);
        }

        //  Суммарное число вершин во всех mDrawCalls.
        public int VertexCount()
        {
            return mDrawCalls.Select(x => x.VertexCount()).Sum();
        }
    }
}

















