﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    class DrawEngine : IDrawEngine
    {
        private Version mVersion;

        //  Буфера рисуются по порядку.
        private List<IBuffer> mBuffers;

        private FontTexture mFontTexture;

        private OpenGL mOpenGL;

        //  Ширина окна в пикселах.
        private double mWidth;

        //  Высота окна в пикселах.
        private double mHeight;

        //  Прокрутка по оси X.
        public double mScrollX;

        //  Прокрутка по оси Y.
        public double mScrollY;

        public string mFontDir;


        public DrawEngine(Version aVersion, string aFontDir)
        {
            mVersion = aVersion;
            mBuffers = new List<IBuffer>();
            mFontTexture = new FontTexture();
            mOpenGL = null;
            mWidth = 0.0;
            mHeight = 0.0;
            mScrollX = 0.0;
            mScrollY = 0.0;
            mFontDir = aFontDir;

            if (!mFontTexture.LoadIndexFile(aFontDir + "\\font.idx"))
            {
                mFontTexture = null;
            }
        }

        public Version GetVersion()
        {
            return mVersion;
        }

        public double GetScrollX()
        {
            return mScrollX;
        }

        public double GetScrollY()
        {
            return mScrollY;
        }

        public void SetScroll(double aX, double aY)
        {
            mScrollX = aX;
            mScrollY = aY;
        }

        //  Разрешаем сглаживание, эффект заметен на майкрософтовском OpenGL, а на NVidia без разницы.
        private void Smooth(OpenGL gl)
        {
            gl.ShadeModel(OpenGL.GL_SMOOTH);
            gl.Hint(OpenGL.GL_POLYGON_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.Enable(OpenGL.GL_POLYGON_SMOOTH);
            gl.Hint(OpenGL.GL_POINT_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.Enable(OpenGL.GL_POINT_SMOOTH);
            gl.Hint(OpenGL.GL_LINE_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.Enable(OpenGL.GL_LINE_SMOOTH);
        }

        protected void SetProjection(OpenGL gl)
        {
            //  Зададим такую проекцию, чтобы координаты подавать в пикселях с учётом mScrollX, mScrollY.
            //  Ось X направлена вправо, ось Y направлена вниз, координата Z всегда 0.
            gl.MatrixMode(OpenGL.GL_PROJECTION);
            gl.LoadIdentity();
            gl.Ortho(mScrollX, mScrollX + mWidth, mScrollY + mHeight, mScrollY, -1, 1);
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();
        }

        public void Initialized(OpenGL gl, double aWidth, double aHeight)
        {
            mOpenGL = gl;
            mWidth = aWidth;
            mHeight = aHeight;
            foreach (var dbuf in mBuffers) dbuf.Init(gl);

            gl.Disable(OpenGL.GL_DEPTH_TEST);
            gl.Enable(OpenGL.GL_BLEND);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_SRC_ALPHA);

            gl.TexEnv(OpenGL.GL_TEXTURE_ENV, OpenGL.GL_TEXTURE_ENV_MODE, OpenGL.GL_MODULATE);

            //  Задаёт цвет, которым очищается экран в gl.Clear.
            gl.ClearColor(1f, 1f, 1f, 1f);

            Smooth(gl);

            if (mFontTexture != null)
            {
                mFontTexture.LoadTextures(gl, mFontDir);
            }
        }

        public void Resized(double aWidth, double aHeight)
        {
            mWidth = aWidth;
            mHeight = aHeight;
        }

        //  Освободить память.
        public void Done()
        {
            foreach (var dbuf in mBuffers) dbuf.Done(mOpenGL);
            mOpenGL = null;
        }

        public void AddDrawBuffer(params IDrawCall[] aDrawCalls)
        {
            if (aDrawCalls == null) return;
            if (aDrawCalls.Length == 0) return;

            IDrawBuffer dbuf = null;
            switch (mVersion)
            {
                case Version.OpenGL10:
                    dbuf = new DrawBuffer10();
                    break;
                case Version.OpenGL11:
                    dbuf = new DrawBuffer11();
                    break;
                case Version.OpenGL15:
                    dbuf = new DrawBuffer15();
                    break;
                case Version.OpenGL30:
                    dbuf = new DrawBuffer30();
                    break;
            }

            foreach (var dc in aDrawCalls) dbuf.Add(dc);
            mBuffers.Add(dbuf);
        }

        public void AddTextBuffer(params ITextDraw[] aTextDraws)
        {
            if (aTextDraws == null) return;
            if (aTextDraws.Length == 0) return;

            ITextBuffer dbuf = null;
            switch (mVersion)
            {
                case Version.OpenGL10:
                    dbuf = new TextBuffer10(mFontTexture);
                    break;
                case Version.OpenGL11:
                    dbuf = new TextBuffer11(mFontTexture);
                    break;
                case Version.OpenGL15:
                    dbuf = new TextBuffer15(mFontTexture);
                    break;
                case Version.OpenGL30:
                    dbuf = new TextBuffer30(mFontTexture);
                    break;
            }

            foreach (var dc in aTextDraws) dbuf.Add(dc);
            mBuffers.Add(dbuf);
        }

        public void Draw(OpenGL gl)
        {
            mOpenGL = gl;

            //  Очистить экран.
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT);

            SetProjection(gl);

            if (mVersion == Version.OpenGL10)
            {
                foreach (var dbuf in mBuffers) dbuf.Draw(gl);
                return;
            }

            bool tca = false;
            gl.EnableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.EnableClientState(OpenGL.GL_COLOR_ARRAY);

            foreach (var dbuf in mBuffers)
            {
                bool tcaNeed = (dbuf is ITextBuffer);
                if (tca != tcaNeed)
                {
                    if (tcaNeed) gl.EnableClientState(OpenGL.GL_TEXTURE_COORD_ARRAY);
                    else gl.DisableClientState(OpenGL.GL_TEXTURE_COORD_ARRAY);
                    tca = tcaNeed;
                }

                dbuf.Draw(gl);
            }

            gl.DisableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.DisableClientState(OpenGL.GL_COLOR_ARRAY);
            if (tca) gl.DisableClientState(OpenGL.GL_TEXTURE_COORD_ARRAY);
        }

        public ITextDraw NewTextDraw()
        {
            return new TextDraw(mFontTexture);
        }

        public string[] FontNames()
        {
            return mFontTexture.FontNames();
        }
    }
}
