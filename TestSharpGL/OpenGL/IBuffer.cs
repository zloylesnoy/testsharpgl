﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    interface IBuffer
    {
        void Init(OpenGL gl);
        void Draw(OpenGL gl);
        void Done(OpenGL gl);
    }
}
