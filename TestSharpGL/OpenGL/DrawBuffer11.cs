﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using SharpGL;

namespace TestSharpGL
{
    //  Буфер вершин, который содержит вершины для нескольких DrawCall.
    //  Формат вершины: { float2 position, byte4 color }.
    //  Эти DrawCall рисуются последовательно и последующие могут закрашивать предыдущие.
    //  Рисуются средствами GL_EXT_vertex_array OpenGL 1.1.
    //  Необходимо вызвать Done для освобождения памяти по окончании работы.
    class DrawBuffer11 : ADrawBuffer, IDrawBuffer
    {
        //  Буфер вершин содержит вершины в формате { float2 xy, byte4 rgba }.
        private IntPtr mVertexBuffer;


        public DrawBuffer11() : base()
        {
            mVertexBuffer = IntPtr.Zero;
        }

        //  Параметр gl не используется и может быть равен null.
        public void Init(OpenGL gl)
        {
            Done(gl);

            //  Временный буфер:
            int size = VertexCount() * cVertexSize;
            var vertices = new byte[size];

            //  Заполняем временный буфер:
            int ofs = 0;
            foreach (var f in mDrawCalls)
            {
                ofs = f.WriteToBuffer(ofs, vertices);
            }

            //  Настоящий буфер выделяем в куче:
            mVertexBuffer = Marshal.AllocHGlobal(size);

            //  Копируем данные из временного буфера в настоящий:
            Marshal.Copy(vertices, 0, mVertexBuffer, size);
        }

        public void Draw(OpenGL gl)
        {
            gl.VertexPointer(2, OpenGL.GL_FLOAT, cVertexSize, mVertexBuffer);
            gl.ColorPointer(4, OpenGL.GL_UNSIGNED_BYTE, cVertexSize, mVertexBuffer + 8);

            foreach (var f in mDrawCalls) f.Draw(gl);
        }

        //  Освободить память. Параметр gl не используется и может быть равен null.
        public void Done(OpenGL gl)
        {
            if (mVertexBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(mVertexBuffer);
                mVertexBuffer = IntPtr.Zero;
            }
        }
    }
}
