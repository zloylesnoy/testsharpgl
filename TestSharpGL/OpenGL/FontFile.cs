﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    partial class FontFile
    {
        private struct SFont
        {
            public string mName;
            public Int16 mUpBorder;     // самый верхний тексел относительно курсора
            public Int16 mDownBorder;   // самый нижний тексел относительно курсора
            public Dictionary<UInt32, Dictionary<UInt32, Int16>> mKerning;
            public Dictionary<UInt32, SGlyphRecord> mGlyphs;
        }
        private List<SFont> mFonts;

        private UInt16 mTexCount;   // число используемых текстур
        private UInt16 mTexLevel;   // если не 0, то width == height == (1 << mTexLevel)
        private float mTexelWidth;  // 1 / (ширина текстур)
        private float mTexelHeight; // 1 / (высота текстур)

        public FontFile()
        {
            Clear();
        }

        public void Clear()
        {
            mFonts = new List<SFont>();
            mTexCount = 0;
            mTexLevel = 0;
            mTexelWidth = 0;
            mTexelHeight = 0;
        }

        public bool LoadIndexFile(string aFileName)
        {
            bool ok = false;
            try
            {
                byte[] data = System.IO.File.ReadAllBytes(aFileName);
                if (data != null) ok = LoadIndexData(data);
            }
            catch
            {
                ok = false;
            }
            if (!ok) Clear();
            return ok;
        }

        //  Возвращает имена поддерживаемых шрифтов.
        //  Номер шрифта - это индекс в этом массиве.
        public string[] FontNames()
        {
            return mFonts.Select(x => x.mName).ToArray();
        }

        //  Возвращает номер шрифта или -1, если шрифт с таким именем не найден.
        public int FontId(string aFontName)
        {
            for (int i=0; i < mFonts.Count; i++)
            {
                if (mFonts[i].mName == aFontName) return i;
            }
            return -1; // Не найден.
        }

        public int FontCount()
        {
            return mFonts.Count;
        }

        public int TextureCount()
        {
            return mTexCount;
        }

        public bool WriteChar(int aFontId, uint aChar, uint aPrevChar, ITextDraw aDraw)
        {
            if (aFontId < 0) return false;
            if (aFontId >= mFonts.Count) return false;
            if (!mFonts[aFontId].mGlyphs.ContainsKey(aChar)) return false;

            int cursorX;
            int cursorY;
            aDraw.GetCursor(out cursorX, out cursorY);
            
            var kerning = mFonts[aFontId].mKerning;
            if (kerning.ContainsKey(aPrevChar))
            {
                var ker = kerning[aPrevChar];
                if (ker.ContainsKey(aChar))
                {
                    cursorX += ker[aChar];
                }
            }

            var glyph = mFonts[aFontId].mGlyphs[aChar];
            if (!glyph.IsSpace())
            {
                int x1 = cursorX + glyph.mOriginX;
                int y1 = cursorY - glyph.mOriginY;
                int x2 = x1 + glyph.mBoxX;
                int y2 = y1 + glyph.mBoxY;

                float u1 = glyph.mTexX * mTexelWidth;
                float v1 = glyph.mTexY * mTexelHeight;
                float u2 = (glyph.mTexX + glyph.mBoxX) * mTexelWidth;
                float v2 = (glyph.mTexY + glyph.mBoxY) * mTexelHeight;

                aDraw.AddVertex(glyph.mTexture, x1, y1, u1, v1);
                aDraw.AddVertex(glyph.mTexture, x2, y1, u2, v1);
                aDraw.AddVertex(glyph.mTexture, x1, y2, u1, v2);
                aDraw.AddVertex(glyph.mTexture, x2, y1, u2, v1);
                aDraw.AddVertex(glyph.mTexture, x1, y2, u1, v2);
                aDraw.AddVertex(glyph.mTexture, x2, y2, u2, v2);
            }

            cursorX += glyph.mShiftX;
            cursorY += glyph.mShiftY;
            aDraw.SetCursor(cursorX, cursorY);

            return true;
        }
    }
}



















