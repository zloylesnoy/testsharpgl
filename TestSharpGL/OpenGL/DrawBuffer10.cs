﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using SharpGL;

namespace TestSharpGL
{
    //  Буфер вершин, который содержит вершины для нескольких DrawCall.
    //  Формат вершины: { float2 position, byte4 color }.
    //  Рисуются посредством glBegin - glEnd из OpenGL 1.0.
    //  Это самый простой, медленный и устаревший способ из всех.
    class DrawBuffer10 : ADrawBuffer, IDrawBuffer
    {
        public void Init(OpenGL gl)
        {
            //  Ничего не делает.
        }

        public void Done(OpenGL gl)
        {
            //  Ничего не делает.
        }

        public void Draw(OpenGL gl)
        {
            foreach (var f in mDrawCalls) f.DrawBeginEnd(gl);
        }
    }
}
