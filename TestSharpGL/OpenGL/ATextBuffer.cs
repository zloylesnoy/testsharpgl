﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    abstract class ATextBuffer
    {
        //  Размер вершины в байтах.
        protected const int cVertexSize = 20;

        protected readonly IFontTexture mFontTexture;

        protected List<ITextDraw> mTextDraws;


        public ATextBuffer(IFontTexture aFontTexture)
        {
            mFontTexture = aFontTexture;
            mTextDraws = new List<ITextDraw>();
        }

        //  Добавлять DrawCall можно только до вызова функции Create.
        public void Add(ITextDraw aTextDraw)
        {
            mTextDraws.Add(aTextDraw);
        }
    }
}
















