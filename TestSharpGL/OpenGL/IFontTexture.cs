﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    interface IFontTexture
    {
        string[] FontNames();
        int TextureCount();
        void UseTexture(OpenGL gl, int aIndex);
        void DontUseTexture(OpenGL gl);
        bool WriteChar(int aFontId, uint aChar, uint aPrevChar, ITextDraw aDraw);
    }
}















