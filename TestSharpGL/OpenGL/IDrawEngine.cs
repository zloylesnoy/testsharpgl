﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Необходимая для работы версия OpenGL.
    enum Version
    {
        OpenGL10,
        OpenGL11,
        OpenGL15,
        OpenGL30
    }

    //  Через этот интерфейс MainWindow обращается к DrawEngine.
    interface IDrawEngine
    {
        void Done();

        Version GetVersion();

        void AddDrawBuffer(params IDrawCall[] aDrawCalls);
        void AddTextBuffer(params ITextDraw[] aTextDraws);
        ITextDraw NewTextDraw();
        string[] FontNames();

        void Initialized(OpenGL gl, double aWidth, double aHeight);
        void Resized(double aWidth, double aHeight);
        void Draw(OpenGL gl);

        double GetScrollX();
        double GetScrollY();
        void SetScroll(double aX, double aY);
    }
}
