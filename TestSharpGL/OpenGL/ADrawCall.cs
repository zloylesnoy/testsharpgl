﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования, соответствует одному вызову функции glDrawArray.
    abstract class ADrawCall
    {
        //  Число вершин.
        private int mVertexCount;

        //  Смещение первой вершины в общем буфере.
        protected int mOffsetInBuffer;

        //  Координаты вершин, X и Y для каждой вершины.
        protected List<float> mXyList;

        //  Цвета вершин, R,G,B и A для каждой вершины.
        protected List<byte> mColorList;

        //  Текущий цвет.
        private byte[] mCurrentColor;


        public ADrawCall()
        {
            mVertexCount = 0;
            mOffsetInBuffer = 0;
            mXyList = new List<float>();
            mColorList = new List<byte>();
            mCurrentColor = new byte[] { 0, 0, 0, 255 };
        }

        public int VertexCount()
        {
            return mVertexCount;
        }

        public int OffsetInBuffer()
        {
            return mOffsetInBuffer;
        }

        //  Записать данные в один общий буфер по смещению aOffsetInBuffer вершин.
        //  Запомнить смещение aOffsetInBuffer, чтобы позже использовать при рисовании.
        public virtual int WriteToBuffer(int aOffsetInBuffer, byte[] aBuffer)
        {
            const int cVertexSize = 12;

            mOffsetInBuffer = aOffsetInBuffer;
            var vc = aOffsetInBuffer * cVertexSize;
            var end = vc + mVertexCount * cVertexSize;
            int v2 = 0;
            int c2 = 0;
            while (vc < end)
            {
                var xs = BitConverter.GetBytes(mXyList[v2++]);
                aBuffer[vc++] = xs[0];
                aBuffer[vc++] = xs[1];
                aBuffer[vc++] = xs[2];
                aBuffer[vc++] = xs[3];

                var ys = BitConverter.GetBytes(mXyList[v2++]);
                aBuffer[vc++] = ys[0];
                aBuffer[vc++] = ys[1];
                aBuffer[vc++] = ys[2];
                aBuffer[vc++] = ys[3];

                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];
                aBuffer[vc++] = mColorList[c2++];
            }

            return aOffsetInBuffer + mVertexCount;
        }

        //  Установить текущий цвет.
        public void SetColor(byte aRed, byte aGreen, byte aBlue, byte aAlpha = 255)
        {
            mCurrentColor[0] = aRed;
            mCurrentColor[1] = aGreen;
            mCurrentColor[2] = aBlue;
            mCurrentColor[3] = aAlpha;
        }

        //  Добавить одну вершину текущего цвета.
        protected void AddVertex(float aX, float aY)
        {
            mVertexCount++;
            mXyList.Add(aX);
            mXyList.Add(aY);
            mColorList.AddRange(mCurrentColor);
        }

        protected void InsideBeginEnd(OpenGL gl)
        {
            int n = mXyList.Count;
            for (int i = 0; i < n; i += 2)
            {
                gl.Color(mColorList[i * 2], mColorList[i * 2 + 1], mColorList[i * 2 + 2], mColorList[i * 2 + 3]);
                gl.Vertex(mXyList[i], mXyList[i + 1]);
            }
        }
    }
}
