﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования, соответствует одному вызову функции glDrawArray(GL_LINES, ...).
    //  Рисует набор отрезков прямых фиксированной толщины. Цвет линий может меняться.
    //  Неизвестно в каком порядке будут рисоваться отрезки из одного набора,
    //  поэтому нельзя чтобы в одном наборе отрезки разного цвета перекрывали друг друга.
    class DrawLines : ADrawCall, IDrawCall
    {
        //  Толщина линии.
        private float mLineWidth;

        //  Текущая позиция для LineTo.
        private float mCurrentX;
        private float mCurrentY;


        public DrawLines(float aLineWidth) : base()
        {
            mLineWidth = aLineWidth;
            mCurrentX = 0;
            mCurrentY = 0;
        }

        public void Draw(OpenGL gl)
        {
            gl.LineWidth(mLineWidth);
            gl.DrawArrays(OpenGL.GL_LINES, OffsetInBuffer(), VertexCount());
        }

        public void DrawBeginEnd(OpenGL gl)
        {
            gl.LineWidth(mLineWidth);
            gl.Begin(OpenGL.GL_LINES);
            InsideBeginEnd(gl);
            gl.End();
        }

        public void AddLine(float aX1, float aY1, float aX2, float aY2)
        {
            AddVertex(aX1, aY1);
            AddVertex(aX2, aY2);
        }

        public void MoveTo(float aX, float aY)
        {
            mCurrentX = aX;
            mCurrentY = aY;
        }

        public void LineTo(float aX, float aY)
        {
            AddLine(mCurrentX, mCurrentY, aX, aY);
            mCurrentX = aX;
            mCurrentY = aY;
        }
    }
}
