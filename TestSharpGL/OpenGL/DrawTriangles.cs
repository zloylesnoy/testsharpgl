﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Команда для рисования, соответствует одному вызову функции glDrawArray(GL_TRIANGLES, ...).
    //  Рисует набор треугольников. Каждая вершина может иметь свой цвет.
    //  Неизвестно в каком порядке будут рисоваться треугольники из одного набора,
    //  поэтому нельзя чтобы в одном наборе треугольники разного цвета перекрывали друг друга.
    class DrawTriangles : ADrawCall, IDrawCall
    {
        //  Текущая позиция для LineTo.
        private float mCurrentX;
        private float mCurrentY;


        public DrawTriangles() : base()
        {
            mCurrentX = 0;
            mCurrentY = 0;
        }

        public void Draw(OpenGL gl)
        {
            gl.DrawArrays(OpenGL.GL_TRIANGLES, OffsetInBuffer(), VertexCount());
        }

        public void DrawBeginEnd(OpenGL gl)
        {
            gl.Begin(OpenGL.GL_TRIANGLES);
            InsideBeginEnd(gl);
            gl.End();
        }

        //  Добавить треугольник текущего цвета.
        public void AddTriangle(float aX1, float aY1, float aX2, float aY2, float aX3, float aY3)
        {
            AddVertex(aX1, aY1);
            AddVertex(aX2, aY2);
            AddVertex(aX3, aY3);
        }

        //  Добавить прямоугольник текущего цвета.
        public void AddRectangle(float aX1, float aY1, float aX2, float aY2)
        {
            AddTriangle(aX1, aY1, aX2, aY1, aX1, aY2);
            AddTriangle(aX1, aY2, aX2, aY1, aX2, aY2);
        }

        //  Добавить линию текущего цвета. Линия это такой тонкий прямоугольник.
        public void AddLine(float aX1, float aY1, float aX2, float aY2, float aWidth = 1)
        {
            float dx = aY2 - aY1;
            float dy = aX1 - aX2;
            double len = Math.Sqrt(dx * dx + dy * dy);
            if (len == 0) return;
            float m = (float)(0.5 * aWidth / len);
            dx *= m;
            dy *= m;

            AddTriangle(aX1 - dx, aY1 - dy, aX1 + dx, aY1 + dy, aX2 + dx, aY2 + dy);
            AddTriangle(aX2 - dx, aY2 - dy, aX2 + dx, aY2 + dy, aX1 - dx, aY1 - dy);
        }

        public void MoveTo(float aX, float aY)
        {
            mCurrentX = aX;
            mCurrentY = aY;
        }

        public void LineTo(float aX, float aY, float aWidth = 1)
        {
            AddLine(mCurrentX, mCurrentY, aX, aY, aWidth);
            mCurrentX = aX;
            mCurrentY = aY;
        }

        //  Добавить эллипс текущего цвета. Эллипс рисуется как aCount треугольников.
        public void AddEllipse(float aCenterX, float aCenterY, float aRadiusX, float aRadiusY, int aCount)
        {
            AddVertex(aCenterX + aRadiusX, aCenterY);
            AddVertex(aCenterX, aCenterY);
            for (int i = 1; i < aCount; i++)
            {
                double angle = 2 * i * Math.PI / aCount;
                float x = (float)(aCenterX + Math.Cos(angle) * aRadiusX);
                float y = (float)(aCenterY + Math.Sin(angle) * aRadiusY);
                AddVertex(x, y);

                //  Следующий треугольник:
                AddVertex(x, y);
                AddVertex(aCenterX, aCenterY);
            }
            AddVertex(aCenterX + aRadiusX, aCenterY);
        }
    }
}
