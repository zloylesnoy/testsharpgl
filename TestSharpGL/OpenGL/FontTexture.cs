﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    class FontTexture : FontFile, IFontTexture
    {
        private uint[] mTextureIds;


        public FontTexture()
        {
            mTextureIds = null;
        }

        public void UseTexture(OpenGL gl, int aIndex)
        {
            uint texture = 0;
            if ((aIndex >= 0) && (aIndex < mTextureIds.Length)) texture = mTextureIds[aIndex];

            gl.Enable(OpenGL.GL_TEXTURE_2D);
            gl.BindTexture(OpenGL.GL_TEXTURE_2D, texture);
        }

        public void DontUseTexture(OpenGL gl)
        {
            gl.BindTexture(OpenGL.GL_TEXTURE_2D, 0);
            gl.Disable(OpenGL.GL_TEXTURE_2D);
        }

        public bool LoadTextures(OpenGL gl, string aDirName)
        {
            //  Генерируем идентификаторы текстур:
            int n = TextureCount();
            if (n <= 0) return false;
            mTextureIds = new uint[n];
            gl.GenTextures(n, mTextureIds);

            //  Загружаем n файлов текстур в формате BMP, восемь бит grayscale:
            bool ok = false;
            try
            {
                for (int t = 0; t < n; t++)
                {
                    string path = aDirName + "\\font" + t + ".bmp";
                    if (!LoadTexture(gl, mTextureIds[t], path)) return false;
                }
                ok = true;
            }
            catch
            {
                ok = false;
            }

            gl.BindTexture(OpenGL.GL_TEXTURE_2D, 0);
            return ok;
        }

        private bool LoadTexture(OpenGL gl, uint aTextureId, string aFilePath)
        {
            byte[] data = System.IO.File.ReadAllBytes(aFilePath);
            if (data == null) return false;

            //  В начале файла идут заголовки BITMAPFILEHEADER (14 байт)
            //  и BITMAPINFOHEADER (40 байт). Другие форматы не поддерживаются.
            if (data.Length < 54) return false;

            if (data[0] != 'B') return false;
            if (data[1] != 'M') return false;
            uint bfOffBits = BitConverter.ToUInt32(data, 10); // смещение пикселов от начала файла
            int biWidth = BitConverter.ToInt32(data, 18); // ширина в пикселах
            int biHeight = BitConverter.ToInt32(data, 22); // высота в пикселах, может быть отрицательная
            if (biHeight < 0) biHeight = -biHeight;
            ushort biBitCount = BitConverter.ToUInt16(data, 28); // бит на пиксел 8 или 24

            uint format;
            switch (biBitCount)
            {
                case 8:
                    format = OpenGL.GL_ALPHA;
                    break;
                case 24:
                    format = OpenGL.GL_RGB;
                    break;
                default:
                    return false;
            }

            if (data.Length < bfOffBits) return false;
            byte[] pixels = new byte[data.Length - bfOffBits];
            Array.Copy(data, bfOffBits, pixels, 0, pixels.Length);

            gl.BindTexture(OpenGL.GL_TEXTURE_2D, aTextureId);
            gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_BASE_LEVEL, 0);
            gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MAX_LEVEL, 0);
            gl.TexImage2D(OpenGL.GL_TEXTURE_2D, 0, format, biWidth, biHeight, 0, format,
                OpenGL.GL_UNSIGNED_BYTE, pixels);

            return true;
        }
    }
}















