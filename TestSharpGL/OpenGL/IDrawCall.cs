﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    //  Буфер вершин работает с командами для рисования через этот интерфейс.
    interface IDrawCall
    {
        //  Сколько нужно вершин в буфере для этой команды.
        int VertexCount();

        //  Записать вершины в один общий буфер по смещению aOffsetInBuffer вершин.
        //  Запомнить смещение aOffsetInBuffer, чтобы позже использовать при рисовании.
        //  Возвращает смещение в буфере конца записанных вершин.
        int WriteToBuffer(int aOffsetInBuffer, byte[] aBuffer);

        //  Выполнить команду.
        void Draw(OpenGL gl);

        //  Выполнить команду, используя glBegin - glEnd.
        void DrawBeginEnd(OpenGL gl);
    }
}
