﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpGL;

namespace TestSharpGL
{
    interface ITextDraw : IDrawCall
    {
        void GetCursor(out int aX, out int aY);
        void SetCursor(int aX, int aY);
        void AddVertex(int aIndex, float aX, float aY, float aU, float aV);

        void SetColor(byte aRed, byte aGreen, byte aBlue, byte aAlpha = 255);
        void Text(int aFontId, string aText);
    }
}
