﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using SharpGL;

namespace TestSharpGL
{
    //  Необходимо вызвать Done для освобождения памяти по окончании работы.
    class TextBuffer11 : ATextBuffer, ITextBuffer
    {
        //  Буфер вершин содержит вершины в формате { float2 xy, byte4 rgba, float2 uv }.
        private IntPtr mVertexBuffer;


        public TextBuffer11(IFontTexture aFontTexture) :
            base(aFontTexture)
        {
            mVertexBuffer = IntPtr.Zero;
        }

        //  Параметр gl не используется и может быть равен null.
        public void Init(OpenGL gl)
        {
            Done(gl);

            //  Суммарное число вершин в буфере:
            int nVertex = mTextDraws.Select(x => x.VertexCount()).Sum();

            //  Временный буфер:
            int size = nVertex * cVertexSize;
            var vertices = new byte[size];

            //  Заполняем временный буфер:
            int nTextures = mFontTexture.TextureCount();
            int ofs = 0;
            foreach (var f in mTextDraws)
            {
                ofs = f.WriteToBuffer(ofs, vertices);
            }

            //  Настоящий буфер выделяем в куче:
            mVertexBuffer = Marshal.AllocHGlobal(size);

            //  Копируем данные из временного буфера в настоящий:
            Marshal.Copy(vertices, 0, mVertexBuffer, size);
        }

        //  Освободить память. Параметр gl не используется и может быть равен null.
        public void Done(OpenGL gl)
        {
            if (mVertexBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(mVertexBuffer);
                mVertexBuffer = IntPtr.Zero;
            }
        }

        public void Draw(OpenGL gl)
        {
            gl.VertexPointer(2, OpenGL.GL_FLOAT, cVertexSize, mVertexBuffer);
            gl.ColorPointer(4, OpenGL.GL_UNSIGNED_BYTE, cVertexSize, mVertexBuffer + 8);
            gl.TexCoordPointer(2, OpenGL.GL_FLOAT, cVertexSize, mVertexBuffer + 12);

            foreach (var f in mTextDraws) f.Draw(gl);
        }
    }
}


















