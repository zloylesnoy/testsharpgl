﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using SharpGL;

namespace TestSharpGL
{
    //  Буфер вершин, который содержит вершины для нескольких DrawCall.
    //  Формат вершины: { float2 position, byte4 color }.
    //  Эти DrawCall рисуются последовательно и последующие могут закрашивать предыдущие.
    //  Рисуются средствами GL_ARB_vertex_buffer_object, OpenGL 1.5.
    //  Необходимо вызвать метод Done для освобождения памяти по окончании работы.
    class DrawBuffer15 : ADrawBuffer, IDrawBuffer
    {
        //  Идентификатор буфера вершин, каждая вершина - это 2 float координаты и 4 byte цвет.
        private uint mBufferId;


        public DrawBuffer15() : base()
        {
            mBufferId = 0;
        }

        public virtual void Init(OpenGL gl)
        {
            Done(gl);

            //  Временный буфер:
            int size = VertexCount() * cVertexSize;
            var vertices = new byte[size];

            //  Заполняем временный буфер:
            int ofs = 0;
            foreach (var f in mDrawCalls)
            {
                ofs = f.WriteToBuffer(ofs, vertices);
            }

            //  Второй временный буфер:
            IntPtr ptr = Marshal.AllocHGlobal(size);

            //  Копируем из первого буфера во второй:
            Marshal.Copy(vertices, 0, ptr, size);

            //  Настоящий буфер это VBO по имени mBufferId:
            uint[] ids = new uint[1];
            gl.GenBuffers(1, ids);
            mBufferId = ids[0];

            //  Копируем данные из второго временного буфера в mBufferId:
            gl.BindBuffer(OpenGL.GL_ARRAY_BUFFER, mBufferId);
            gl.BufferData(OpenGL.GL_ARRAY_BUFFER, size, ptr, OpenGL.GL_STATIC_DRAW);
            gl.BindBuffer(OpenGL.GL_ARRAY_BUFFER, 0);

            //  Временные буферы больше не нужны:
            Marshal.FreeHGlobal(ptr);
        }

        public virtual void Draw(OpenGL gl)
        {
            gl.BindBuffer(OpenGL.GL_ARRAY_BUFFER, mBufferId);
            gl.VertexPointer(2, OpenGL.GL_FLOAT, cVertexSize, IntPtr.Zero);
            gl.ColorPointer(4, OpenGL.GL_UNSIGNED_BYTE, cVertexSize, IntPtr.Zero + 8);
            foreach (var f in mDrawCalls) f.Draw(gl);
            gl.BindBuffer(OpenGL.GL_ARRAY_BUFFER, 0);
        }

        //  Освободить память.
        public virtual void Done(OpenGL gl)
        {
            if (mBufferId != 0)
            {
                gl.DeleteBuffers(1, new uint[1] { mBufferId });
                mBufferId = 0;
            }
        }
    }
}
