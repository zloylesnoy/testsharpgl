﻿Рисование на плоскости средствами OpenGL.

Используется библиотека https://github.com/dwmkerr/sharpgl/
В консоли NuGet необходимо выполнить:
PM> Install-Package SharpGL
PM> Install-Package SharpGL.WPF

                Test1  Test2
DrawEngine10    27.8    35.6
DrawEngine11    31.7    48.9
DrawEngine15    31.8    49.6

Test1 - на ноутбуке, 15000 кадров, полный экран.
Test2 - на десктопе, 100000 кадров, полный экран.













