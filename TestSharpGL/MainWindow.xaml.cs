﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SharpGL;
using SharpGL.SceneGraph;

//  В консоли NuGet необходимо выполнить:
//  PM> Install-Package SharpGL
//  PM> Install-Package SharpGL.WPF

namespace TestSharpGL
{
    public partial class MainWindow : Window
    {
        private int mFrameCount;
        private DateTimeOffset mStartTime;
        private IDrawEngine mDrawEngine;


        public MainWindow()
        {
            InitializeComponent();

            var img = mOpenGL.Content as Image;
            if (img != null) img.Stretch = Stretch.None;

            mFrameCount = 0;
            mStartTime = DateTimeOffset.Now;

            mDrawEngine = new DrawEngine(Version.OpenGL15, "..\\..\\..\\font");
            CreateScene();
        }

        //  Здесь мы строим сцену и загружаем её в mDrawEngine.
        private void CreateScene()
        {
            var tr = new DrawTriangles();
            tr.SetColor(0xFF, 0x00, 0x00);
            tr.AddTriangle(100, 100, 0, 100, 100, 0);
            tr.SetColor(0x00, 0x00, 0xFF, 0x80); // полупрозрачный
            tr.AddRectangle(70, 70, 150, 150);
            tr.SetColor(0x00, 0xFF, 0xFF, 0x80); // полупрозрачный
            tr.AddEllipse(100, 200, 50, 80, 32);

            var tr1 = new DrawTriangles();
            tr1.SetColor(0x00, 0x80, 0x00);
            tr1.MoveTo(400, 0);
            for (var i = 0; i < 5000; i++)
            {
                tr1.LineTo(400 + 100 * (float)Math.Sin(i * 0.1), i, 5);
            }

            //  tr1 и ls рисуют одинаковую зелёную синусоиду разными способами: треугольниками и линиями.

            var ls = new DrawLineStrip(5);
            ls.SetColor(0x00, 0x80, 0x00);
            for (var i = 0; i < 5000; i++)
            {
                ls.LineTo(600 + 100 * (float)Math.Sin(i * 0.1), i);
            }

            var tr2 = new DrawTriangles();
            for (var i = 0; i < 5000; i++)
            {
                tr2.SetColor((byte)(0xFF - (i & 0xFF)), 0x00, (byte)(i & 0xFF), 0x80);
                tr2.LineTo(500 + 100 * (float)Math.Cos(i * i * 0.0001), i, 5);
            }

            for (var i = 0; i < 400; i++)
            {
                tr2.SetColor(0, (byte)(Math.Cos(i * 0.3) * 100 + 28), 0x80, 0xFF);
                float x = 900 + 200 * (float)Math.Cos(i * 0.5);
                float y = 400 + 200 * (float)Math.Sin(i * 0.07);
                if (i == 0) tr2.MoveTo(x, y);
                else tr2.LineTo(x, y, 3);
            }

            mDrawEngine.AddDrawBuffer(tr, tr1, ls, tr2);

            //  Выводим имена всех поддерживаемых шрифтов этими самыми шрифтами.
            //  Сначала выводим чёрную тень, затем поверх неё серую надпись.
            var text = mDrawEngine.NewTextDraw();
            var shadow = mDrawEngine.NewTextDraw();
            text.SetColor(0x80, 0x80, 0xA0);
            shadow.SetColor(0x00, 0x00, 0x20);
            string[] fonts = mDrawEngine.FontNames();
            for (var i = 0; i < fonts.Length; i++)
            {
                text.SetCursor(30, 30 * (i+1));
                text.Text(i, fonts[i]);
                shadow.SetCursor(31, 30 * (i + 1) + 1);
                shadow.Text(i, fonts[i]);
            }
            mDrawEngine.AddTextBuffer(shadow, text);
        }

        private void OpenGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {
            mDrawEngine.Initialized(args.OpenGL, mOpenGL.ActualWidth, mOpenGL.ActualHeight);
        }

        private void OpenGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            mDrawEngine.Draw(args.OpenGL);

            //  Скроллим по Y:
            mDrawEngine.SetScroll(mDrawEngine.GetScrollX(), mScrollY);

            mFrameCount++;
            //  Каждые 100 кадров выводим статистику:
            if (mFrameCount % 100 != 0) return;
            var fps = mFrameCount / (DateTimeOffset.Now - mStartTime).TotalSeconds;
            mInfo.Text = String.Format("{0} frames, FPS={1:F3}", mFrameCount, fps);
        }

        private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            mDrawEngine.Resized(mOpenGL.ActualWidth, mOpenGL.ActualHeight);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            mDrawEngine.Done();
        }

        Point ptCursorPos = new Point(0, 0);
        double mScrollY;

        private void MOpenGL_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                return;

            Point pt = e.GetPosition(mOpenGL);
            mScrollY += ptCursorPos.Y - pt.Y;
            ptCursorPos = pt;
        }

        private void MOpenGL_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ptCursorPos = e.GetPosition(mOpenGL);
        }
    }
}
